ARG NODE_VERSION=lts

FROM node:${NODE_VERSION}-alpine

MAINTAINER Damien DUBOEUF <duboeuf.damien@gmail.com>

run apk add  --no-cache \
    git shadow \
    && rm -rf /var/cache/apk/* 

ENV USER_ID=1000
ENV USER_GID=1000

COPY docker-user-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-user-entrypoint.sh"]
CMD ["node"]