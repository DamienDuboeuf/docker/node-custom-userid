# Node Image Node with custom User ID

[![pipeline status](https://gitlab.com/DamienDuboeuf/docker/node-custom-userid/badges/master/pipeline.svg)](https://gitlab.com/DamienDuboeuf/docker/node-custom-userid/-/commits/master)

Its docker image node with custom system user id

## Usages

```bash
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:lts sh # For Node lts
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:23 sh # For Node 23
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:22 sh # For Node 22
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:21 sh # For Node 21
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:20 sh # For Node 20
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:19 sh # For Node 19
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:18 sh # For Node 18
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:17 sh # For Node 17
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:16 sh # For Node 16
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:15 sh # For Node 15
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:14 sh # For Node 14
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:13 sh # For Node 13
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:12 sh # For Node 12
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:10 sh # For Node 10
```

use specifique tagged version

```
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:lts-v1.1.3 sh # For Node lts
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:23-v1.1.3 sh # For Node 23
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:22-v1.1.3 sh # For Node 22
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:21-v1.1.3 sh # For Node 21
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:20-v1.1.2 sh # For Node 20
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:19-v1.1.0 sh # For Node 19
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:18-v1.1.2 sh # For Node 18
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:17-v1.1.2 sh # For Node 17
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:16-v1.1.2 sh # For Node 16
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:15-v1.1.2 sh # For Node 15
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:14-v1.1.2 sh # For Node 14
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:13-v1.1.2 sh # For Node 13
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:12-v1.1.2 sh # For Node 12
docker run -ti registry.gitlab.com/damienduboeuf/docker/node-custom-userid:10-v1.1.2 sh # For Node 10
```